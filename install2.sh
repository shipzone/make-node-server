cd ~
mkdir workspace
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.26.1/install.sh | bash
. ~/.bashrc
bash -c "nvm install stable"
bash -c "nvm alias default stable"
bash -c "n=$(which node);n=${n%/bin/node}; chmod -R 755 $n/bin/*; sudo cp -r $n/{bin,lib,share} /usr/local"
curl -L https://raw.githubusercontent.com/c9/install/master/install.sh | bash
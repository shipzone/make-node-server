#Make-Node-Server

Create a new server, then login as root and run the following command:

    bash -c "$(curl -s https://raw.githubusercontent.com/pushrocks/make-node-server/master/install.sh)"

##What does this script do?

It updates your machine
It installs some important packages
It makes a safeuser and adds it to the sudo group
It installs the latest node via nvm
It installs the latest cloud9 ssh node package.
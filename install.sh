#!/bin/bash -e
set -e
# ----- install basic native packages ------------------------------------------
apt-get update
apt-get upgrade -y
apt-get install -y build-essential
apt-get install -y git
apt-get install -y nginx
apt-get install -y fail2ban

# ----- Start checking environment ---------------------------------------------
read -p "Do you want to create a safe user for node programs to run on?" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo #force new line
read -p "Provide a name for the safe user: " safeusername
echo 'now creating ssafeusernamefe user'
adduser $safeusername
usermod -a -G sudo $safeusername
/bin/su - $safeusername -c "curl -L https://raw.githubusercontent.com/pushrocks/make-node-server/master/install2.sh | bash"
fi
